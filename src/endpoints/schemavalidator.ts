import express from 'express';
import bodyParser from 'body-parser';
import jwt from 'express-jwt';
import user from '../config';
import Ajv from 'ajv';

const router = express.Router();
router.use(bodyParser.json());

const schema = {
  type: 'object',
  oneOf: [
    {
      properties: {
        squadName: { type: 'string' },
        homeTown: { type: 'string' },
        formed: { type: 'number' },
        active: { type: 'boolean' },
      },
      required: ['squadName', 'homeTown', 'formed', 'active'],
      additionalProperties: false,
    },
    {
      properties: {
        name: { type: 'string' },
        age: { type: 'number' },
        secretIdentity: { type: 'string' },
        active: { type: 'boolean' },
        powers: { type: 'array' },
      },
      required: ['name', 'age', 'secretIdentity', 'active', 'powers'],
      additionalProperties: false,
    },
  ],
};

const ajv = new Ajv({ allErrors: true });
router.patch(
  '/schemavalidator',
  jwt({
    secret: user.secret,
    credentialsRequired: false,
    getToken: function(req) {
      const token = req.headers.token;
      if (token) {
        return token;
      } else {
        return null;
      }
    },
  }),
  (req, res) => {
    const data = req.body;
    const validate = ajv.compile(schema);
    const valid = validate(data);
    if (!valid) {
      console.log(validate.errors);
      res.send(400);
    } else {
      res.status(200).send({ msg: 'the schema is acceptable' });
    }
  },
);

export default router;
