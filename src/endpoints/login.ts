import express from 'express';
import jwt from 'jsonwebtoken';
import user from '../config';
import bodyParser from 'body-parser';
import dotenv from 'dotenv';

dotenv.config();

const router = express.Router();
router.use(bodyParser.json());

router.post('/login', (req, res) => {
  const username = req.body.username;
  const password = req.body.password;
  const userData = { username: username, password: password };
  if (username === user.username && password === user.password) {
    jwt.sign({ userData }, user.secret, (_err: any, token: any) => {
      res.json({ token });
    });
  } else {
    res.json('Invalid Credentials');
  }
});

export default router;
