import express from 'express';
import morgan from 'morgan';
import login from './endpoints/login';
import schemavalidator from './endpoints/schemavalidator';

const app = express();
const port = 3000;

app.use(morgan('combined'));
app.use(login);
app.use(schemavalidator);

export const startApp = async (): Promise<void> => {
  app.use('*', function(_req, res) {
    res.status(404).send('404 Not Found');
  });

  app.listen(port, () => {
    // tslint:disable-next-line:no-console
    console.log(`Example app listening on port ${port}!`);
  });
};
